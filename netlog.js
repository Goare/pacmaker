"use strict";
var page = require('webpage').create(),
    system = require('system'),
    _      = require('lodash'),
    fs     = require('fs'),
    address;

var domains = [];

if (system.args.length === 1) {
    console.log('Usage: netlog.js <some URL>');
    phantom.exit(1);
} else {
    address = system.args[1];

    page.settings.userAgent = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36';
    page.settings.resourceTimeout = 3000;

    page.onResourceRequested = function (req) {
        // console.log('requested: ' + JSON.stringify(req, undefined, 2));
        if (!req) return;
        console.log('requested req: ' + JSON.stringify(req, undefined, 1));

        var url = req.url;
        console.log('requested: ' + url);



        if (url && /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/.test(url))
        {

          url = url.match(/^(https?:\/\/)?([^\/]*)(\.[\da-z-]+?\.[a-z\.]{2,6})\/?(.*)/)[3];

          domains.push(url);
        }

    };

    page.open(address, function (status) {
        if (status !== 'success') {
            console.log('FAIL to load the address');
        }

        domains = _.uniq(domains);
        domains = _.filter(domains, function(domain){
          return !(/\.cn$/.test(domain));
        })
        console.log('---------------------------------------------');
        console.log(domains);
        console.log('---------------------------------------------');

        // write the new added domains to domains.txt
        fs.write('./domains.txt', fs.read('./domains.txt') + '\n' + domains);

        _.map(domains, function(domain){
          console.log(domain);
        })
        phantom.exit();
    });
}
